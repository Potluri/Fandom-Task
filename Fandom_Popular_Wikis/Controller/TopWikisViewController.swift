//
//  TopWikisViewController.swift
//  Fandom_Popular_Wikis
//
//  Created by Potluri on 7/22/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import UIKit
import SDWebImage
import SafariServices

class TopWikisViewController: UIViewController {
    
    var items : [Item]? {
        get{
            return ServiceRequest.shared.items
        }
    }
    
    var reloadData = false {
        willSet{
            if newValue {
                fetchDataforTableView()
            }
        }
    }
    
    lazy var tableView : UITableView! = {
        var tv = UITableView()
        tv.delegate = self
        tv.dataSource = self
        tv.rowHeight = UITableViewAutomaticDimension
        tv.estimatedRowHeight = 200
        tv.allowsSelection = true
        tv.bounces = false
        tv.alwaysBounceVertical = false
        tv.separatorStyle = .none
        tv.register(CustomTVCell.self, forCellReuseIdentifier: Constants.cellID)
        tv.backgroundColor = Constants.tableViewBackgroundColor
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        self.navigationItem.title = "Top Wikis"
        view.addSubview(tableView)
        view.addConstraintsWithFormat("H:|[v0]|", views: tableView)
        view.addConstraintsWithFormat("V:|[v0]|", views: tableView)
        getUserDeviceTrait()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        fetchDataforTableView()
    }
    
    func getUserDeviceTrait(){
        let horizontalSize = self.traitCollection.horizontalSizeClass
        
        switch horizontalSize {
        case UIUserInterfaceSizeClass.compact:
            Constants.fontSizeMedium = 24
            Constants.fontSizeRegular = 12
        default:
            Constants.fontSizeMedium = 34
            Constants.fontSizeRegular = 17
        }
    }
    
    
    
    func fetchDataforTableView(){
        let batch = ServiceRequest.shared.topWikis?.currentBatch ?? 0
        ServiceRequest.shared.getTopWikis(batch: batch+1) { (success) in
            if !success {
                print("serviceRequestFailed")
                DispatchQueue.main.async {
                    self.present(Alerts().notSuccessful, animated: false, completion: nil)
                }
            } else {
                DispatchQueue.main.async {
                    self.reloadData = false
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        if traitCollection.horizontalSizeClass == .compact {
            Constants.fontSizeMedium = 24
            Constants.fontSizeRegular = 18
        } else {
            Constants.fontSizeMedium = 34
            Constants.fontSizeRegular = 25
        }
    }
}

extension TopWikisViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellID) as! CustomTVCell
        if indexPath.row == (items?.count ?? 0) - 1 {
            reloadData = true
        }
        if let data = items?[indexPath.row]{
            cell.title = data.title
            cell.articles = data.articles
            if data.imageURL != "" && data.imageURL != nil {
                cell.mageView.sd_setImage(with: URL(string: data.imageURL!), placeholderImage: nil, options: [.cacheMemoryOnly], completed: nil)
            } else {
                cell.mageView.image = UIImage(named: Constants.noImage)
            }
            cell.videos = data.videos
            cell.descn = data.description
            cell.discussion = data.discussions
            let view = UIView()
            view.backgroundColor = UIColor.clear
            cell.selectedBackgroundView = view
            cell.layoutSubviews()
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let str = items?[indexPath.row].pageLink
        guard let stri = str, stri != "" else { print("AlertViewShowingNOURL"); return}
        let url = URL(string: stri)!
        let svc = SFSafariViewController(url: url)
        present(svc, animated: true, completion: nil)
    }
}
