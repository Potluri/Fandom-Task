//
//  Alerts.swift
//  Fandom_Popular_Wikis
//
//  Created by Potluri on 7/22/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import UIKit


class Alerts {
    lazy var notReachable : UIAlertController = {
        let alertViewController = UIAlertController(title: "No Internet", message: "NO internet Access. Please Check your connection", preferredStyle: .alert)
        alertViewController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        return alertViewController
    }()
    
    lazy var notSuccessful : UIAlertController = {
        let alertViewController = UIAlertController(title: "No Internet", message: "Unable to reach website. Please Check your connection", preferredStyle: .alert)
        alertViewController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        return alertViewController
    }()
}

