//
//  Constants.swift
//  Fandom_Popular_Wikis
//
//  Created by Potluri on 7/22/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import UIKit

struct Constants {
    static let fontRegular = "Rubik-Regular"
    static let fontMedium = "Rubik-Medium"
    static let cellID = "cellID"
    static let url = "http://www.wikia.com/api/v1/Wikis/List?expand=1&limit=25&batch="
    static let barColor = UIColor(red: 0.18, green: 0.24, blue: 0.31, alpha: 1)
    static let tableViewBackgroundColor = UIColor(red:0.93, green:0.94, blue:0.95, alpha:1.0)
    static let fontColorForDescription = UIColor(red:0.83, green:0.85, blue:0.87, alpha:1.0)
    static let noImage = "no-image-available"
    static var fontSizeRegular = CGFloat(0)
    static var fontSizeMedium = CGFloat(0)
}
