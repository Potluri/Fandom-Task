//
//  ServiceRequest.swift
//  Fandom_Popular_Wikis
//
//  Created by Potluri on 7/22/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import UIKit
import SwiftyJSON

class ServiceRequest {
    
    static let shared = ServiceRequest()
    
    var topWikis: TopWikis?
    var items = [Item]()
    
    
    func setTopWikis(json: JSON){
        print("Set top wikis is under execution")
        let batches = json["batches"].intValue
        let currentBatch = json["currentBatch"].intValue
        let next = json["next"].intValue
        let array = json["items"].array!
        var items = [Item]()
        for i in array {
            let title = i["title"].string!
            let description = i["desc"].string!
            let imageUrl = i["image"].string!
            let discussions = i["stats"]["discussions"].intValue
            let videos = i["stats"]["videos"].intValue
            let pageLink = i["url"].string!
            let articles = i["stats"]["articles"].intValue
            let item = Item(image: nil, title: title, pageLink: pageLink, description: description, imageURL: imageUrl, discussions: discussions, videos: videos, articles: articles)
            items.append(item)
        }
        self.topWikis = TopWikis(batches: batches, currentBatch: currentBatch, next: next)
        self.items.append(contentsOf: items)
    }
    
    func getTopWikis(batch: Int = 1, completion: @escaping (_ success: Bool)-> ()){
        let address = Constants.url + String(batch)
        let url = URL(string: address)!
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                completion(false)
                print(error!.localizedDescription)
            }  else {
                guard let data = data else { return }
                do {
                    let json = try JSON(data: data)
                    self.setTopWikis(json: json)
                    completion(true)
                } catch let error {
                    print(error.localizedDescription)
                    completion(false)
                }
            }
        }.resume()
    }
}

