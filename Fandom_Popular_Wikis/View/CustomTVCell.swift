//
//  CustomTVC.swift
//  Fandom_Popular_Wikis
//
//  Created by Potluri on 7/22/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import UIKit

class CustomTVCell: UITableViewCell {
    
    var mainImage : UIImage?
    var title : String?
    var descn: String?
    var discussion: Int?
    var videos : Int?
    var articles: Int?
    
    let titleTextView: UITextView = {
        let tv = UITextView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.font = UIFont.preferredFont(forTextStyle: .headline)
        tv.textColor = Constants.barColor
        tv.font = UIFont(name: Constants.fontMedium, size: Constants.fontSizeMedium)
        tv.isScrollEnabled = false
        return tv
    }()
    
    let descriptionTextView: UITextView = {
        let tv = UITextView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.font = UIFont.preferredFont(forTextStyle: .body)
        tv.textColor = Constants.fontColorForDescription
        tv.font = UIFont(name: Constants.fontRegular, size: Constants.fontSizeRegular)
        tv.textAlignment = .left
        tv.isScrollEnabled = false
        return tv
    }()
    
    let view: UIView = {
        let vi = UIView()
        vi.backgroundColor = Constants.tableViewBackgroundColor
        vi.translatesAutoresizingMaskIntoConstraints = false
        return vi
    }()
    
    let subView: UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor.white
        vi.layer.cornerRadius = 8
        vi.translatesAutoresizingMaskIntoConstraints = false
        return vi
    }()
    
    var mageView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .white
        iv.clipsToBounds = false
        iv.layer.cornerRadius = 10
        iv.layer.shadowColor = UIColor.black.cgColor
        iv.layer.shadowOffset = CGSize(width: 0, height: 4)
        iv.layer.shadowOpacity = 1
        iv.contentMode = .scaleToFill
        return iv
    }()
    
    let articleButton = CustomTVCell.buttonForTitle("Article", imageName: "icArticle")
    let discussionsButton: UIButton = CustomTVCell.buttonForTitle("Discussions", imageName: "icDiscussions")
    let videosButton: UIButton = CustomTVCell.buttonForTitle("Videos", imageName: "icVideo")
    
    static func buttonForTitle(_ title: String, imageName: String) -> UIButton {
        let button = UIButton()
        button.isEnabled = true
        button.setTitle(title, for: UIControlState.normal)
        button.setTitleColor(UIColor(red: 143/255, green: 150/255, blue: 163/255, alpha: 1), for: UIControlState.normal)
        button.setImage(UIImage(named: imageName), for: UIControlState.normal)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 8, 0, 0)
        button.titleLabel?.font = UIFont(name: Constants.fontRegular, size: Constants.fontSizeRegular)
        return button
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        stackView.removeAllArrangedSubviews()
    }
    
    var stackView : UIStackView = {
        let sv = UIStackView()
        sv.backgroundColor = UIColor.red
        sv.axis = .horizontal
        sv.alignment = .fill
        sv.distribution = .fillEqually
        return sv
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(view)
        
        view.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        view.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        view.addSubview(subView)
        subView.addSubview(titleTextView)
        subView.addSubview(descriptionTextView)
        subView.addSubview(mageView)
        subView.addSubview(stackView)
        
        addConstraintsWithFormat("H:|-40-[v0]-20-|", views: subView)
        addConstraintsWithFormat("V:|-40-[v0]-20-|", views: subView)
        
        subView.addConstraintsWithFormat("H:|-(-20)-[v0(150)]-10-[v1]-10-|", views: mageView,titleTextView)
        subView.addConstraintsWithFormat("H:|-10-[v0]-20-|", views: descriptionTextView)
        subView.addConstraintsWithFormat("V:|-10-[v0]-10-[v1]-10-[v2]-10-|", views: titleTextView,descriptionTextView,stackView)
        subView.addConstraintsWithFormat("V:|-(-30)-[v0(150)]-10-[v1]-10-[v2(50)]|", views: mageView,descriptionTextView,stackView)
        subView.addConstraintsWithFormat("H:|-10-[v0]-10-|", views: stackView)
        self.bringSubview(toFront: contentView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        titleTextView.text = title ?? "No title available"
        descriptionTextView.text = descn ?? "No description available"
        
        if mainImage != nil {
            mageView.image = mainImage
        }
        
        if articles?.convertNumToString() != "0" {
            articleButton.setTitle(articles!.convertNumToString(), for: UIControlState.normal)
            stackView.addArrangedSubview(articleButton)
        }
        if discussion?.convertNumToString() != "0" {
            discussionsButton.setTitle(discussion!.convertNumToString(), for: UIControlState.normal)
            stackView.addArrangedSubview(discussionsButton)
        }
        if videos?.convertNumToString() != "0" {
            videosButton.setTitle(videos!.convertNumToString(), for: UIControlState.normal)
            stackView.addArrangedSubview(videosButton)
        }
    }

}
