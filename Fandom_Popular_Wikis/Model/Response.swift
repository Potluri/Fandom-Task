//
//  Response.swift
//  Fandom_Popular_Wikis
//
//  Created by Potluri on 7/22/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import UIKit

struct TopWikis {
    var batches: Int
    var currentBatch: Int
    var next : Int
}

struct Item {
    var image: UIImage?
    var title: String
    var pageLink: String
    var description: String
    var imageURL: String?
    var discussions: Int
    var videos: Int
    var articles: Int
}


