//
//  Int+ConvertTo.swift
//  Fandom_Popular_Wikis
//
//  Created by Potluri on 7/22/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import Foundation


extension Int {
    func convertNumToString() -> String{
        var str = ""
        if self/1_000_000_000 >= 1{
            str += "\(Int(self/1_000_000_000))B"
        } else if self/1_000_000 >= 1{
            str += "\(Int(self/1_000_000))M"
        } else if self/1_000 >= 1 {
            str += "\(Int(self/1_000))K"
        } else {
            str += "\(self)"
        }
        return str
    }
}
